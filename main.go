package main

import (
	"github.com/Sirupsen/logrus"
	"app/route"
	"encoding/json"
	"log"
	"github.com/gorilla/mux"
)
// Get Version
func getVersion(w http.ResponseWriter, r *http.Request){

}

func init() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})
}

func main() {
	//Init Router
	r := mux.NewRouter()
	//Route Handlers / Endpoints
	r.HandleFunc("/api/version", GetVersion).Method("GET")
	// router := route.Init()
	// router.Start(":80")
	log.Fatal(http.ListenAndServe(":80", r))
}
